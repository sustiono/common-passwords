require 'nokogiri'
require 'open-uri'

puts 'test'
doc = Nokogiri::HTML(open('https://en.wikipedia.org/wiki/List_of_the_most_common_passwords'))
table_rows = doc.css('#mw-content-text > div > table:nth-child(6) > tbody > tr')
passwords = []
table_rows.each_with_index do |tr, index|
  if index > 0
    tr.css('td').each_with_index do |td, index|
      if index > 0
        passwords << td.text.gsub(/[\r\n]+/, '')
        File.open('common_passwords.txt', 'w+') do |f|
          f.puts(passwords)
        end
      end
    end
  end
end

puts passwords
